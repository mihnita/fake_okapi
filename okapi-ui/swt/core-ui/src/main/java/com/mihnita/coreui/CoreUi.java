package com.mihnita.coreui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class CoreUi {
	public int doSomething(Shell parent, String text) {
		Label label = new Label(parent, SWT.NONE);
		label.setText(text);
		System.out.println("Method in " + this.getClass());
		return 42;
	}
}
