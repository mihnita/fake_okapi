package com.mihnita.propertiesui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class PropertiesFilterUi {
	public int doSomething(Shell parent, String text) {
		Label label = new Label(parent, SWT.NONE);
		label.setText(text);
		System.out.println("Method in " + this.getClass());
		return 42;
	}
}
