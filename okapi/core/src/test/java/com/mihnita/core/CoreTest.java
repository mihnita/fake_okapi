package com.mihnita.core;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class CoreTest {
	@Test
	public void simpleTest() {
		System.out.println("Testing, " + this.getClass());
		assertEquals(42, new Core().doSomething());
	}
}
