package com.mihnita.core;

import java.text.NumberFormat;
import java.util.Properties;
import java.util.TreeSet;

public class Core {

	public static void main(String[] args) {
		new Core().doSomething();
	}

	public int doSomething() {
		System.out.println("Method in " + this.getClass());

		System.out.println("=================================");

		Runtime runtime = Runtime.getRuntime();
		NumberFormat nf = NumberFormat.getInstance();
		System.out.println("runtime.availableProcessors(): " + runtime.availableProcessors());
		System.out.println("runtime.freeMemory()  : " + nf.format(runtime.freeMemory()/1024/1024) + " MB");
		System.out.println("runtime.totalMemory() : " + nf.format(runtime.totalMemory()/1024/1024) + " MB");
		System.out.println("runtime.maxMemory()   : " + nf.format(runtime.maxMemory()/1024/1024) + " MB");
		System.out.println("runtime.version()     : " + runtime.version());

		System.out.println("=================================");

		Properties properties = System.getProperties();
		TreeSet<String> sortedKeys = new TreeSet<>(properties.stringPropertyNames());
		for(String key : sortedKeys) {
			System.out.println(key + " = " + properties.getProperty(key));
		}

		return 42;
	}
}
