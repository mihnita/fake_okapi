package com.mihnita.textmodification;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class TextModificationStepTest {
	@Test
	public void simpleTest() {
		System.out.println("Testing, " + this.getClass());
		assertEquals(42, new TextModificationStep().doSomething());
	}
}
