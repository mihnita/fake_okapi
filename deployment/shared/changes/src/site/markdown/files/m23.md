# Changes from M22 to M23

<!-- MACRO{toc} -->

## Applications

* Rainbow

    * Added the Inconsistency Check Step to the pre-defined Quality Check pipeline.

* CheckMate

    * Fixed [issue #358](https://gitlab.com/okapiframework/okapi/-/issues/358): The Check Document button now works in all cases.

## Filters Plugin for OmegaT

    * Added .mxliff as one of the default extensions for XLIFF.
    * Fixed [issue #364](https://gitlab.com/okapiframework/okapi/-/issues/364): .sdlxliff files with UTF-8 BOM open now.

## Steps

    * Added the Inconsistency Check Step: a way to flag entries with
      the same source that have different targets or the entries with
      the same target that have different source.

* Rainbow Translation Kit Creation Step

    * Added a `libVersion` attribute in the manifest
      indicating the version of the library used to create the manifest.
    * Add option to use encapsulation notation (`<bpt>`/`<ept>`/`<ph>`/`<it>`)
      for inline codes in OmegaT tkits.

* LanguageTool Step

    * Updated the library to version 2.2.

* Encoding Conversion Step

    * Fixed [issue #318](https://gitlab.com/okapiframework/okapi/-/issues/318): ASCII characters in NCR form are now
      un-escaped except for `"`, `'`, `&`, `<` and `>`.

* Search and Replace Step

    * Fixed [issue #183](https://gitlab.com/okapiframework/okapi/-/issues/183): Added simple log of the replacements.
    * Fixed [issue #362](https://gitlab.com/okapiframework/okapi/-/issues/362): Step for Terminology fixes on translation candidates.

* Quality Check Step

    * Resolved [issue #357](https://gitlab.com/okapiframework/okapi/-/issues/357): Added function to detect blacklisted terms.
    * Improved ITS LQI support.

## Filters

* IDML Filter

    * Implemented [issue #356](https://gitlab.com/okapiframework/okapi/-/issues/356): By default spread above the threshold
      cause an error. The option allows to skip without error.

* XML Filter

    * Continued implementation of ITS 2.0.
    * Fixed [issue #361](https://gitlab.com/okapiframework/okapi/-/issues/361): MIME type can be different in sub-classes of XMLFilter.

* HTML5-ITS Filter

    * Continued implementation of ITS 2.0.

* JSON Filter

    * Resolved [issue #360](https://gitlab.com/okapiframework/okapi/-/issues/360): The use of the key for the `resname` value
      is now optional.

* XLIFF Filter

    * Continued implementation of ITS 2.0.
    * Fixed [issue #364](https://gitlab.com/okapiframework/okapi/-/issues/364): Woodstox XML parser is now always used.
    * Alt-trans with empty target are now skipped.
    * Added support for the `<tool>` and `<phase>`
      elements as well as the `state-qualifier` attribute.

* OpenXML Filter

    * Fixed [issue #291](https://gitlab.com/okapiframework/okapi/-/issues/291): Sub-documents are now processed in correct order.
    * Fixed [issue #319](https://gitlab.com/okapiframework/okapi/-/issues/319): 'squishable' tests has been changed.

* Simplification Filter

    * Fixed [issue #355](https://gitlab.com/okapiframework/okapi/-/issues/355): parameters of sub-filter are properly read
      in the cases where the primary filter uses a sub-filter.

* XINI Filter

    * Fixed a case where placeholders were being renumbered
      incorrectly when reading a XINI file.

## Libraries

* Verification Library

    * Fixed issue with non-initialized start/end variable when
      checking patterns from the target.
    * Added support for sub-document in Quality Checker library.

## General

    * Continued implementation of ITS 2.0 in XLIFFWriter,
      XLIFFContent, etc.
    * Fixed [issue #352](https://gitlab.com/okapiframework/okapi/-/issues/352): XMLWriter now throw OkapiIOException if an
      error occurs.
    * Updated XLIFF Writer to match ITS/XLIFF official mapping
      (http://www.w3.org/International/its/wiki/XLIFF_1.2_Mapping).
    * Added the experimental lib-concurrent package to improve
      multi-threaded pipelines. See
      [ThrededWorkQueue Step page](http://okapiframework.org/wiki/wiki/index.php?title=ThreadedWorkQueue_Step)
      for details.
