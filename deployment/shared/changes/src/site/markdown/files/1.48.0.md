# Changes from 1.47.0 to 1.48.0

<!-- MACRO{toc} -->

## Core

* Added X
* Fixed [issue #1234](https://gitlab.com/okapiframework/okapi/-/issues/1234):
  Some description of the issue fixed. \
  Can use space + backslash at the end for a line break. \
  It generates a `<br />` without ending the paragraph or the list item.
* Deprecated Y

## Connectors

* ABC Connector

    * Fixed X
    * Improved Y

## Filters

* ABC Filter

    * Fixed X
    * Improved Y

* DEF Filter

    * Deprecated Z

## Libraries

* ABC Library

    * Fixed X
    * Improved Y
    * Deprecated Z

## Steps

* ABC Step

    * Fixed X
    * Improved Y
    * Deprecated Z

## TMs

* ABC TM

    * Fixed X
    * Improved Y
    * Deprecated Z

## Applications

* Tikal

    * Fixed X
    * Improved Y
    * Deprecated Z

* CheckMate

    * Fixed X

* Rainbow

    * Fixed X

* Ratel

    * Fixed X

* Serval

    * Fixed X

## OSes

* macos

    * Did something for macos

* All

    * Did something for all systems

## Installation

* Did something 1
* Did something 2

## General

* Did something 1
* Did something 2

## Plugins

* OmegaT

    * Did something

* Trados Utilities

    * Did something even better
