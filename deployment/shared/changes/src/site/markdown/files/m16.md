# Changes from M15 to M16

## Applications

* Rainbow

    * Rainbow now returns a non-zero error code if the command-line ends
      with an error ([Issue #201](https://gitlab.com/okapiframework/okapi/-/issues/201))

* Tikal

    * Fixed [issue #215](https://gitlab.com/okapiframework/okapi/-/issues/215) by always adding the HTML filter to the mapping
      because now several filters use it as sub-filter.

* Ratel

    * Added an option to allow backward compatibility with Java regular
      expressions.
    * Improved the UI to separate standard options from extensions.
    * Changed the UI to start with an empty document with a single
      default language map.

## Filters

* TMX Filter

    * Added an option to stop processing when encountering invalid `<tu>`
      elements. By default invalid `<tu>` elements
      are excluded.

* PHP Content Filter

    * Fixed the [issue #204](https://gitlab.com/okapiframework/okapi/-/issues/204): duplicated code when a string is an
      inline code with concatenation and linebreak.

* Properties Filter

    * Fixed the default rule for Java inline codes so escaped is
      supported.
    * Fixed the issue where custom filter configurations for
      sub-filters were not loaded properly.

* Table Filter

    * Added handling of 2 escaping modes for qualifiers in CSV
      files:
        * by duplication of the qualifier (e.g. "translate=""yes""")
        * by backslash escaping (e.g. "translate=\"yes\"")
    * Added a new parameter to CSV filter: \
      `int escapingMode` \
      `int ESCAPING_MODE_DUPLICATION = 1 (default)` \
      `int ESCAPING_MODE_BACKSLASH = 2`
    * Added the "CSV escaping mode" group to the parameters editor.
    * Fixed a bug with disabling CSV-related elements in the
      parameters editor.

* TS Filter

    * Fixed the default rule for Java inline codes so escaped is
      supported.

* PO Filter

    * Fixed the default rule for Java inline codes so escaped is
      supported.

* IDML Filter

    * Added the option to skip large spread files.

* HTML Filter

    * Improve insersion of encoding declaration when missing
      (support for XHTML and custom XML)
    * Improve support for translate='yes\no'

* XLIFF Filter

    * Fixed segment id for case where first segment id is "0" and it
      is preceeded by a non-segment part.

* XINI Filter

    * Fixed parameters editing error.

    * Added Drupal filter, this filter allows you to process selected
      node content from a Drupal host. The filter is Alpha for now.

## Steps

* Rainbow Translation Kit Creation Step

    * Fixed the issue of the missing tab before existing target in
      the Translation Table package format.

* Rainbow Translation Kit Merging Step

    * Added option to output the merged files in a specified
      directory (vs. the one defined in the manifest).

* Segmentation Step

    * Added 3 options for treating already segmented text.

* Leveraging Step

    * Added the option to not query if the entry has already a
      candidate equals or above a given score.

* XSL Transformation Step

    * Added the option to specify a custom XPath class in addition
      to the custom XSL Transformer class.
    * Fixed the step so a temporary output is used when the input
      and output have the same path.

## Libraries

* Fixed duplicated ID values in `TextFragment.insert()` when the
  inserted fragment had overlaps.
* Segmenter now uses ICU to implement SRX. \
  **Important: This may affect segmentation with your existing SRX
  files. For example `\w` now matches also accented letters (the
  normal Java `\w` does not). \
  See [SRX and Java](http://www.opentag.com/okapi/wiki/index.php?title=SRX_and_Java)
  for details. Use the new "use Java regular expressions" options to force backward compatibility.**

