# Changes from M29 to M30

<!-- MACRO{toc} -->

## Filters

* HTMLEncoder

    * Quote Mode Options Added: Added the ability to configure quote
      escaping rules to filter configs: `UNESCAPED`, `ALL`,
      `NUMERIC_SINGLE_QUOTES`, `DOUBLE_QUOTES_ONLY`.

* MIF Filter

    * Support FrameMaker 2015 files.

* TS Filter

    * The text units are now extracted with the flag to preserve
      whitespace set.

* XLIFF Filter

    * Fixed [issue #521](https://gitlab.com/okapiframework/okapi/-/issues/521): `<phase>` elements will no
      longer be reordered when processing a file with the filter.
    * Fixed [issue #207](https://gitlab.com/okapiframework/okapi/-/issues/207): Added the "Preserve whitespace on 'default'"
      option, which forces the filter to preserve whitespace when the `xml:space="default"`
      attribute is present, as if the `"preserve"` value was
      present instead.
    * Fixed [issue #539](https://gitlab.com/okapiframework/okapi/-/issues/539): Implemented support for `translate`
      in `<group>` and `<bin-unit>`.

* XLIFF2 Filter

    * Added an initial and experimental implementation of a filter for
      XLIFF v2.0 files.

* HTML Filter

    * When merging in a right-to-left target language, the `dir="rtl"`
      attribute will be added to the `<html>` element.

* HTML5 Filter

    * When merging in a right-to-left target language, the `dir="rtl"`
      attribute will be added to the `<html>` element.

* ITS Filter

    * Fixed the issue when inline empty elements with two tags like `<span></span>`Text
      was output incorrectly. (See contribution from the
      [FREME project](https://github.com/freme-project/e-Internationalization/issues/30)).
    * Improved the pre-defined rules for the Android Strings filter
      configuration.

* OpenXML Filter

    * Support for `.dotx`, `.dotm`, `.ppsx`, `.ppsm`, `.potx`, `.potm`, `.ppsx`,
      `.xltm` files has been added.
    * Fix [issue #297](https://gitlab.com/okapiframework/okapi/-/issues/297): richly styled text in Excel spreadsheet cells will
      now produce text units containing inline codes that represent formatting.
    * Improve handling of Smart Tags.
    * The filter will no longer expose redundant copies of text stored
      in "Alternate Content" blocks for translation. The filter will
      expose the primary version of the text for translation and strip the
      fallback content from the target document. Fallback content can be
      regenerated, if necessary, by opening the document in Office.
    * Fix [issue #524](https://gitlab.com/okapiframework/okapi/-/issues/524): a bug that caused duplicate TextUnit IDs to be
      generated in some cases involving nested content.
    * Fix a bug that hid hyperlink URLs from translation.
    * Fix [issue #526](https://gitlab.com/okapiframework/okapi/-/issues/526): Word documents containing charts that contained
      entities could be corrupted on merge.
    * Exposed several new options in the Rainbow UI, including the
      ability to treat tabs and line breaks as characters, the handling of
      soft hyphens, and whether to automatically accept document
      revisions.
    * Fix [issue #532](https://gitlab.com/okapiframework/okapi/-/issues/532): Worksheet names in Excel documents can now be
      exposed for translation using the new "Translate Sheet Names"
      option.
    * Fix [issue #533](https://gitlab.com/okapiframework/okapi/-/issues/533): Add an option to expose hyperlinks stored in `.rels`
      files for translation.

* OpenOffice Filter

    * Formula results are no longer extracted for translation in ODS
      files.
    * Automatically-generated numbers are no longer extracted for
      translation.
    * Document metadata is now only extracted if the `extractMetadata`
      configuration parameter is enabled. This parameter is enabled by
      default.

* Subfilters

    * Fix [issue #530](https://gitlab.com/okapiframework/okapi/-/issues/530) - JSON content could be corrupted when processed
      with a subfilter.
    * Fixed issues related to subfilters that call additional
      subfilters.

## Steps

* Id-Based Aligner Step

    * Add an option to allow alignment based on TextUnit ID, rather than
      resource name. This is useful for aligning formats where no resource
      name exists, but the TextUnit ordering is known to be stable between files.

* Localizables Checker Step

    * Checks dates, times and numbers and flags text units where the
      target instance is either missing or not localized properly.

* Language Tool Step

    * Enhance to provide morphologically valid bilingual term and black
      term checking if LanguageTool supports a stemmer for the locale.
      Otherwise resort to full word comparisons.

## General

    * Fix [Issue #523](https://gitlab.com/okapiframework/okapi/-/issues/523): XLIFFWriter will now preserve properties set on
      empty targets.
    * Changed XLIFF 2 library to version 1.1.1.
    * Upgrade ICU4J to version 57.1.
    * Split Quality Check step into independent steps: character, general,
      length, patterns, inline codes etc.
