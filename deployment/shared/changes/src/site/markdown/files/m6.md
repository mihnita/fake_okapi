# Changes from M5 (0.5.1) to M6

## Installation

* Updated the Macintosh distributions with application bundles for
  Rainbow and Ratel.
* Changed the Macintosh distributions to GunZIP files to preserve
  executable flag of the shell scripts.

## Applications

* Rainbow

    * Translation Package Creation:
        * Fixed the issue where pre-segmented RTF output was losing
          referents in target.
        * Fixed the deletion of the empty TMX files when the package is
          zipped.
    * Added English-India in the locales list.
    * Fixed bug where steps using 3 input lists for more than 3 input
      files were getting null values instead of raw documents.
    * Added support for plugins for steps, filters and parameters
      editors. Just drop the JAR in the `dropins` folder.
    * Updated the way the utilities menu is stored.
    * Replaced the "URI Conversion" utility by a pre-define pipeline
      using the "URI Conversion" step.

* Tikal

    * Added support for plugins for filters and parameters editors. Just
      drop the JAR in the `dropins` folder.

## Steps

* Format Conversion Step

    * Fixed the issue where monolingual segmented input was not
      output properly in tab-delimited format.

* Added the "Desegmentation" Step

* Added the "URI Conversion" Step

* Search and Replace Step
    * Added Import/Export functions to the dialog box of the "Search and
      Replace" step

## Libraries

* Changed QueryManager

    * Allow code changes in target for the non-segmented queries.
    * Prevents exact matches to have the target codes "adjusted"
      from the source.
    * Added `setReferentCopies()` to `GenericSkeletonWriter` to allow
      correct output for writers refering more than once to the referents
      (e.g. when creating pre-segmented RTF with source and target).
    * Moved `lib-plugins` to common.

## Translation resources

* Added in SimpleTM an option for code content and order difference
  between query and source text

## Filters

* HTML Filter

    * Added support for inline codes using regular expressions.

* Table Filter

    * Fixed [issue #124](https://gitlab.com/okapiframework/okapi/-/issues/124) where part of the copy of the file
      configuration was dropped for TSV files whn creating package for
      XLIFF.

* TTX Filter

    * Fixed [issue #130](https://gitlab.com/okapiframework/okapi/-/issues/130) where empty TargetLanguage attributes were
      not updated with the target language code.

* XML Filter

    * Improved the pre-defined configuration for Android resources
      files.
    * Fixed [issue #128](https://gitlab.com/okapiframework/okapi/-/issues/128): help example for codeFinder: `count=1`
      is now `count.i=1`.

