# Changes from M10 to M11

## Filters Plugin for OmegaT

* Added the Transifex Filter

    * Transifex projects can be edited
      directly from OmegaT now (write-rights needed to save the
      translations)

## Applications

* Rainbow

    * Added the "Export Batch Configuration" command to create
      single-file configuration that can be transported and re-installed
      elsewhere.
    * Added the "Install Batch Configuration" command to create a set of
      local files from a batch configuration.
    * Added the "Translation Kit Creation" pre-defined pipeline, to
      create translation kit. (BETA). When done, this will replace the
      "Translation Package Creation" utility.
    * Added the "Translation Kit Post-Processing" pre-defined pipeline,
      to merge back extracted file (BETA). When done, this will replace
      the "Translation Package Post-Processing" utility.
    * Changed the "Translation Package Creation" utility to not output
      non-translatable entries in OmegaT package.
    * Fixed which page was called for the command-line help.

* Tikal

    * Fixed the `-lfc` command so it includes custom configurations as
      weel as the default ones.
    * Added the `-noalttrans` option to the `-x` command.
    * Fixed problem with application's path containing spaces. This
      fixes [issue #162](https://gitlab.com/okapiframework/okapi/-/issues/162).
    * Integrated Transifex, MIF and Archive filters.

* CheckMate

    * Added the check for absolute maximum length.
    * Added option to automatically recheck the documents when they
      change.

## Steps

* Added the "Rainbow Translation Kit Creation" Step

    * to create translation package in various format from a pipeline.

* Added the "Rainbow Translation Kit Merging" Step

    * to merge back extracted files.

* XML Characters Fixing Step

    * Implemented support for decimal and hexadecimal NCRs in
      addition to raw characters.
    * Added the "Id-Based Copy" step, to copy the source text from one
      file into the target of another for the entries with the same id.

* Scoping Report Step

    * Many changes have been implemented: better support for GMX and
      Okapi categories, option for custom templates, etc.

* RTF Conversion

    * Added warning when a character cannot be encoding in the
      output encoding.
    * Added option to update the encoding declaration in XML/HTML
      files when possible.

## Filters

* Added the MIF Filter (Beta)

* Added the Rainbow Translation Kit Filter

    * to process translation packages.

* Added the Transifex Filter

    * to process remote Transifex projects.

* Added the Archive Filter

    * to process any files inside a zip or jar file.

* HTML Filter

    * Fixed the case where non-quoted one-word translatable
      attributes could be merged back as non-quoted multi-words. This
      resolves [issue #126](https://gitlab.com/okapiframework/okapi/-/issues/126).

* TS Filter

    * Added resname for group (`<context>` elements)
    * Changed to use Woodsox Stax parser instead of the defautl VM
      parser.

* TTX Filter

    * Improved handling of unsegmented content to mimick TagEditor's
      behavior closer: Leading whitespace characters are now excluded
      from the entries. This addresses [issue #164](https://gitlab.com/okapiframework/okapi/-/issues/164).
    * Changed to use Woodsox Stax parser instead of the defautl VM
      parser.

* PO Filter

    * Added mapping for msgctxt to the `context` property.
    * Added the option to protect approved entries (i.e not empty
      and not fuzzy).

* TMX Filter

    * Changed to use Woodsox Stax parser instead of the defautl VM
      parser.

* XLIFF Filter

    * Fixed issue with out-of-segment inline codes collapsing in
      previous empty segmented target entry.
    * Fixed the bug were the approved property was not writeable.
      Now you can add, delete or modify it.
    * Changed to use Woodsox Stax parser instead of the defautl VM
      parser.

* OpenXML Filter

    * Fixed the problem of `<` and/or `>` in text boxes causing
      merging error. This resolves [issue #142](https://gitlab.com/okapiframework/okapi/-/issues/142).

* Table Filter

    * Fixed the issue with incorrectly setting the inline code
      finder rules.

* Plain Text Filter

    * Fixed the issue with incorrectly setting the inline code
      finder rules.

## Connectors

* Apertium Connector

    * Added a timeout option.

## Libraries

* Fixed the short search cases in NGramTokenizer/Analyzer.
  This fixes [issue #159](https://gitlab.com/okapiframework/okapi/-/issues/159).
* Added Transifex client library.

